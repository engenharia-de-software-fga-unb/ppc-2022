function text_parts = find_text_parts(searchable_text,begin_enclosure,end_enclosure)

t = strfind(searchable_text,begin_enclosure);
text_parts = cell(1,length(t));
for j=1:length(text_parts)
	t1 = t(j) + length(begin_enclosure);
	t2 = strfind(searchable_text(t1:end),end_enclosure)-2;
	text_parts{j} = searchable_text(t1+[0:t2(1)]);
end
if length(text_parts)==0
	text_parts = '';
elseif length(text_parts)==1
	text_parts = text_parts{1};
end