url = 'https://matriculaweb.unb.br';
pasta = './tabelas/';

switch curso
	case 1
		url_master = 'https://matriculaweb.unb.br/graduacao/fluxo.aspx?cod=6131';   % Eletronica
	case 2
		url_master = 'https://matriculaweb.unb.br/graduacao/fluxo.aspx?cod=6009';   % Engenharias
	case 3
		url_master = 'https://matriculaweb.unb.br/graduacao/fluxo.aspx?cod=60836';  % Aerospacial
	case 4
		url_master = 'https://matriculaweb.unb.br/graduacao/fluxo.aspx?cod=6297';   % Automotiva
	case 5
		url_master = 'https://matriculaweb.unb.br/graduacao/fluxo.aspx?cod=6289';   % Energia
	case 6
		url_master = 'https://matriculaweb.unb.br/graduacao/fluxo.aspx?cod=6360';   % Software
end

begin_enclosures = {"<div style='padding:5px'>",'<a href='};
end_enclosures = {'</div>','>'};

[a,b] = system(['curl -s --insecure ' url_master]);
%b = fileread('Fluxo_Eletronica_Mat_Web.html');
b = find_text_parts(b,begin_enclosures{1},end_enclosures{1});
pages = find_text_parts(b,begin_enclosures{2},end_enclosures{2});
pages = unique(pages);

for j = 1:length(pages)
	%%% Baixar o plano de ensino da disciplina
	[a,b] = system(['curl -s --insecure ' url pages{j}]);
	
	%%% Descobrir o nome da disciplina
	titulo = find_text_parts(b,'font size=-1','(<a href=');
	titulo = find_text_parts(titulo,'<br>Disciplina: ','<');
	%%% Arrumar nome do arquivo, retirando espaços em branco, hifens e acentos
	titulo = strrep(titulo,' ','_');
	titulo = strrep(titulo,'_-_','_');
	titulo = strrep(titulo,'á','a'); titulo = strrep(titulo,'Á','A');
	titulo = strrep(titulo,'é','e'); titulo = strrep(titulo,'É','E');
	titulo = strrep(titulo,'í','i'); titulo = strrep(titulo,'Í','I');
	titulo = strrep(titulo,'ó','o'); titulo = strrep(titulo,'Ó','O');
	titulo = strrep(titulo,'ú','u'); titulo = strrep(titulo,'Ú','U');
	titulo = strrep(titulo,'à','a'); titulo = strrep(titulo,'À','A');
	titulo = strrep(titulo,'â','a'); titulo = strrep(titulo,'Â','A');
	titulo = strrep(titulo,'ê','e'); titulo = strrep(titulo,'Ê','E');
	titulo = strrep(titulo,'î','i'); titulo = strrep(titulo,'Î','I');
	titulo = strrep(titulo,'ô','o'); titulo = strrep(titulo,'Ô','O');
	titulo = strrep(titulo,'û','u'); titulo = strrep(titulo,'Û','U');
	titulo = strrep(titulo,'ã','a'); titulo = strrep(titulo,'Ã','A');
	titulo = strrep(titulo,'õ','o'); titulo = strrep(titulo,'Õ','O');
	titulo = strrep(titulo,'ç','c'); titulo = strrep(titulo,'Ç','C');
	codigo_end = find(titulo=='_',1);
	titulo = [titulo((codigo_end+1):end) '_' titulo(1:(codigo_end-1))];
	
	page_start = find_text_parts(b,'<!-- PERÍODO -->','<!-- TABELA MEIO -->');
	page_start = strrep(page_start,'Disciplina - Listagem de Ementa/Programa','');
	page_start = strrep(page_start,'<center><br><br><b>Período Atual</b></center>','');
	page_start = strrep(page_start,'<center>2016/1</center>','');
	page_start = strrep(page_start,'<td class="padrao" align="center" valign="middle">','<td class="padrao" align="left" valign="middle">');
	c = find_text_parts(b,begin_enclosures{1},end_enclosures{1});
	
	pagina_html = [page_start c];
	
	elementos_latex = {'<br>Disciplina:','<br>('; ...
					'<td><b>Órgão:</b> </td><td>','</td></tr>'; ...
					'<b>Código:</b> </td><td>','</td></tr>'; ...
					'<td><b>Denominação:</b> </td><td>','</td></tr>'; ...
					'<td><b>Nível:</b> </td><td>','</td></tr>'; ...
					'<td><b>Vigência:</b> </td><td>','</td></tr>'; ...
					'<b>Pré-req:</b> </td><td class=PadraoMenor>','</td></tr>'; ...
					'<b>Ementa:</b> </td><td class=PadraoMenor><p align=justify>','</P></td></tr>'; ...
					'<b>Programa:</b> </td><td class=PadraoMenor><p align=justify>','</P></td></tr>'; ...
					'<b>Bibliografia:</b> </td><td class=PadraoMenor><p align=justify>','</P></td></tr>'};
	nomes_elementos = {'Disciplina', 'Órgão', 'Código', 'Denominação', 'Nível', 'Vigência', ...
					'Pré-req','Ementa','Programa','Bibliografia'};
	
	arq_latex = ['\begin{scriptsize}' char(10) '\begin{longtable}{p{15cm}}' char(10) ...
				 '\toprule' char(10) '\rowcolor[gray]{0.9}' char(10) '{\textbf{\textsc{' ...
				 find_text_parts(pagina_html, elementos_latex{4,1}, elementos_latex{4,2}) ...
				 '}$\quad$(' ...
				 find_text_parts(pagina_html, elementos_latex{3,1}, elementos_latex{3,2}) ...
				 ')}}' char(10) '\\' char(10) '\midrule' char(10) '\\' char(10) '\textbf{Ementa} ' char(10) '\\' char(10) ...
				 find_text_parts(pagina_html, elementos_latex{8,1}, elementos_latex{8,2}) ...
				 char(10) '\\' char(10) '\\' char(10) '\textbf{Programa}' char(10) '\\' char(10) ...
				 find_text_parts(pagina_html, elementos_latex{9,1}, elementos_latex{9,2}) ...
				 char(10) '\\' char(10) '\\' char(10) '\textbf{Bibliografia}' char(10) '\\' char(10) ...
				 find_text_parts(pagina_html, elementos_latex{10,1}, elementos_latex{10,2}) ...
				 char(10) '\\' char(10) '\midrule' char(10) '\textbf{Pre-Requisito:}' char(10) ...
				 find_text_parts(pagina_html, elementos_latex{7,1}, elementos_latex{7,2}) ...
				 char(10) '\\' char(10) '\bottomrule' char(10) '\end{longtable}' char(10) '\end{scriptsize}'
				];

	arq_latex = strrep(arq_latex,'<br><br>','<br>');
	arq_latex = strrep(arq_latex,'<br /><br />','<br />');
	arq_latex = strrep(arq_latex,'<br><br>','<br>');
	arq_latex = strrep(arq_latex,'<br /><br />','<br />');
	arq_latex = strrep(arq_latex,'<br><br>','<br>');
	arq_latex = strrep(arq_latex,'<br /><br />','<br />');
	arq_latex = strrep(arq_latex,'<br><br>','<br>');
	arq_latex = strrep(arq_latex,'<br /><br />','<br />');
	arq_latex = strrep(arq_latex,'<br>',[char(10) '\\' char(10)]);
	arq_latex = strrep(arq_latex,'<br />',[char(10) '\\' char(10)]);
	arq_latex = strrep(arq_latex,'[ELIBRARY]','(eBrary)');
	arq_latex = strrep(arq_latex,'[EBRARY]','(eBrary)');
	arq_latex = strrep(arq_latex,'[ebrary]','(eBrary)');
	arq_latex = strrep(arq_latex,'[SCIENCEDIRECT]','(SCIENCEDIRECT)');
	arq_latex = strrep(arq_latex,'[OPEN ACCESS]','(open access)');
	arq_latex = strrep(arq_latex,'[Open Access]','(open access)');
	arq_latex = strrep(arq_latex,'[IEEEXPLORE]','(IEEEXPLORE)');
	arq_latex = strrep(arq_latex,'[BOOKBOON]','(BOOKBOON)');
	arq_latex = strrep(arq_latex,'&','\&');
	arq_latex = strrep(arq_latex,'^','\^');
	filewrite([pasta titulo '.tex'], arq_latex);
end