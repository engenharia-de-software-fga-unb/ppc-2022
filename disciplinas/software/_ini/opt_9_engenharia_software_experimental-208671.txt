Engenharia de Software Experimental

[Ementa]
Princípios e técnicas para experimentação em Engenharia de Software.
Planejamento e condução de experimentos.
Revisão de processos de medição e técnicas estatísticas para análise de dados.
Análise crítica de artigos científicos recentes.

[Programa]
01. Princípios gerais de metodologia e experimentação
- Paradigmas científicos de produção de conhecimento
- Abordagens, métodos e técnicas
- Planejamento
- Abordagens qualitativas, quantitativas e mistas
- Ferramentas de gerenciamento de referências
- Análise de dados e publicação de trabalhos
02. Experimentação em Engenharia de Software
- Tipos de problemas e questões metodológicas
- O projeto de software como um experimento
- Fontes de incerteza em engenharia de software
- Métricas de software: objetivas, subjetivas, métricas para processo, projeto e produto, métricas de código-fonte (qualidade interna).
- Etnografia em engenharia de software
03. Projeto e definição de pesquisa
- Definição de questão e pesquisa
- Revisão crítica da literatura relacionada
- Proposição de hipótese
- Coleta de dados e definição de variáveis dependentes e independentes.
04. Aplicação de técnicas estatísticas para análise de dados
- Análise fatorial
- Correlações
- Análise multivariada
- Testes de hipóteses

[Bibliografia Básica]
JURISTO, N.; MORENO, A.M. Basics of Software Engineering Experimentation. Berlim: The Kluwer International Series in Software Engineering. 2000.
WOHLIN, C., RUNESON, P., HÖST, M., OHLSSON, M. C., REGNELL, B., WESSLÉN, a. Experimentation in Software Engineering: An Introduction. Massachusets: Kluwer Academic Publishers, USA. 1ª Edição. 2000. 
ANDRADE, Maria Margarida D. Introdução à metodologia do trabalho científico: elaboração de trabalhos na graduação, 10ª edição. Grupo GEN, 2012. 9788522478392. Disponível em: https://integrada.minhabiblioteca.com.br/#/books/9788522478392/. (E-book)

[Bibliografia Complementar]
GIL, Antonio C. Como Elaborar Projetos de Pesquisa, 6ª edição. Grupo GEN, 2017. 9788597012934. Disponível em: https://integrada.minhabiblioteca.com.br/#/books/9788597012934/. (E-book)
GIL, Antonio Carlos. Como elaborar projetos de pesquisa. 5. ed. São Paulo: Atlas, 2010. xvi, 184 p. ISBN 9788522458233.
YIN, Robert K. Estudo de caso: planejamento e métodos. 5. ed. Porto Alegre, RS: Bookman, 2015. xxix, 290 p. ISBN 9788582602317.
BASILI, V. The Role of Experimentation in Software Engineering: Past, Current, and Future, In: Proceedings of the 18th International Conference on Software Engineering (ICSE). Berlin: ICSE, 2006.
BERNDTSSON, Mikael. Thesis projects: a guide for students in computer science and information systems. 2nd ed. London: Springer, c2008. xiii, 158 p. ISBN 9781848000087.
GIL, Antonio Carlos. Estudo de caso: fundamentação científica, subsídios para a coleta e análise de dados, como redigir o relatório. São Paulo: Atlas, 2009. 148 p. ISBN 9788522455324.
TRAVASSOS, G. H.; GUROV, D. E AMARAL, E.A. G. Introdução à Engenharia de Software Experimental. Programa de Engenharia de Sistemas. COPPE. Rio de Janeiro.2002.
ORAM, A. e WILSON, G. (Orgs.). Making Software What Really Works, and Why We Believe It. Sebastopol, CA: O’Reilly Media, Inc. 2011.


[Pré-requisito]
Probabilidade e Estatística Aplicada à Engenharia E
Qualidade de Software 1.
