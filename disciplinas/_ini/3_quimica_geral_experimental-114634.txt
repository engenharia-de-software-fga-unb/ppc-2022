Química Geral Experimental

[Ementa]
Caracterização da natureza e do papel das investigações experimentais em química.
Estudo de medidas e de algarismos significativos.
Desenvolvimento de habilidades de manuseio de aparelhos volumétricos, de sistemas de filtração, de sistemas de destilação e de processos químicos.
Desenvolvimento do espírito de observação, análise e interpretação de fenômenos químicos.
Estudo experimental de processos químicos elementares.

[Programa]
1. Noções Básicas sobre Segurança no Trabalho em Laboratório de Química.
2. Apresentação de Equipamentos, Materiais e Vidrarias a Serem Utilizados Durante a Execução dos Experimentos Propostos.
3. Realização de Experimentos Representativos sobre Temas que Reforcem o Aprendizado de Conceitos Fundamentais de Química, tais como: Reação Química; Equilíbrio Químico; Cinética Química; Conceitos de Ácidos e Bases; Oxi-Redução; Termoquímica; Eletroquímica; etc.
4. Execução de Experimentos Simples e que Correlacionem o Aspecto Conceitual ao Cotidiano no que se Refere a Análise e/ou Preparação de Materiais, tais como: Polímeros, Pigmentos e Corantes, Metais, Alimentos, Bebidas, Medicamentos, Cosméticos, Detergentes.

[Bibliografia Básica]
Roteiro de Experimentos elaborados por professores do Instituto de Química da UnB.
Periódicos: Journal of Chemical Education; Química Nova; Química Nova na Escola; outros.
Silva, R. R.; Bocchi, N.; Rocha-Filho, R.; "Introdução à Química Experimental"; McGraw-Hill, São Paulo, 1990.

[Bibliografia Complementar]
Chrispino, A ; "Manual de Química Experimental"; Ática, São Paulo, 1990.
Livros Diversos de Química Geral - Teoria e Prática.

[pre-requisito]
Disciplina sem pre-requisito
