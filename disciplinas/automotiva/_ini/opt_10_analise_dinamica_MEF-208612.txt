Análise Dinâmica Método dos Elementos Finitos
[Ementa]
1- Introdução; 
2- Noções sobre método dos elementos finitos; 
3- Fundamentos de sistemas com um grau de liberdade;
4- Sistemas com vários graus de liberdade;
5- Deterinaição de frequências natuais e formas modais;
6- Método da superposição modal;
7- Métodos de integração numérica;
8- Análise dinâmica via método dos elementos finitos;
9. Pacote computacional para modelagem via método dos elementos finitos (ANSYS);
[Programa]
1. Introdução
1.1. Considerações gerais
1.2. Modelos de análise
1.3. Objetivo e âmbito da disciplina
2.	Noções sobre método dos elementos finitos
2.1.  Introdução
2.2. Método da Rigidez
2.3. Método de Raylegh Ritz
2.4. Interpolação
3. Fundamentos de sistemas com um grau de liberdade
3.1. Vibração livre 
3.2. Vibração forçada
4.	Sistemas com vários graus de liberdade
4.1. Formulação das equações de movimento
4.2. Caracterização do sistema: matrizes de massa, amortecimento e rigidez
4.3. Formulação da equação de movimento com base na equação de Lagrange
5. Deterinaição de frequências natuais e formas modais
5.1. Vibrações livres não-amortecidas
5.2. Métodos de determinação das frequências e modos de vibração
6. Método da superposição modal
6.1. Coordenadas Principais
6.2. Equações desacopladas de movimento
6.3. Análise da resposta via superposição modal
7. Métodos de integração numérica
7.1. Análise da resposta dinâmica passo à passo
7.2. Método de Newmark
7.3. Método de Runge-Kutta
7.4. Estudo de convergência dos algoritmos
8. Análise dinâmica via método dos elementos finitos
8.1. Modelagem da estrutura via elementos finitos
8.2. Determinação de frequências naturais e modos de vibração
9. Pacote computacional para modelagem via método dos elementos finitos (ANSYS)
9.1. Análise Modal
9.2. Análise Harmônica
9.3. Análise Transiente
9.4. Estudos de convergência (malha e passo de tempo)
[Bibliografia Básica]
COOK, Robert Davis. Concepts and applications of finite element analysis. 4th ed. Hoboken, NJ: John Wiley \& Sons, c2002
CRAIG, Roy R.; KURDILA, Andrew. Fundamentals of structural dynamics. 2nd ed. New Jersey: Wiley, 2006.
SORIANO, Humberto Lima. Elementos finitos: formulação e aplicação na estática e dinâmica das estruturas. Rio de Janeiro: Ciência Moderna, 2009
[Bibliografia Complementar]
BENAROYA, Haym; NAGURKA, Mark L. Mechanical vibration: analysis, uncertainties, and control 3rd ed. Boca Raton: CRC Press, c2010
INMAN, D. J. Vibration with control. John Wiley \& Sons, 2006
MEIROVITCH, Leonard. Analytical methods in vibrations. New York: Macmillan Publishing, c1967
MOAVENI, Saeed. Finite element analysis: theory and application with ANSYS . 3rd ed. Upper Saddle River: Pearson Prentice Hall, c2008
RAO, S. S. Vibrações mecânicas. 4. ed. São Paulo: Pearson Prentice Hall, 2008
[Pré-requisito]
Análise Estrutural Método dos Elementos Finitos
