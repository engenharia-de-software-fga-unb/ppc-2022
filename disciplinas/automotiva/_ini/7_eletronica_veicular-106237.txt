Eletrônica Veicular
[Ementa]
1. Introdução aos sistemas elétricos veiculares;
2. Componentes automotivos básicos;
3. Baterias automotivas;
4. Sistemas de carregamento;
5. Sistema de partida do motor;
6. Sistema de ignição;
7. Controle da injeção e gerenciamento do motor;
8. Sistema de iluminação automotiva;
9. Sistemas elétricos do chassi;
10.	Diagnóstico de bordo;
11.	Introdução a redes veiculares;
[Programa]
1.	Introdução aos sistemas elétricos veiculares
1.1. Evolução dos sistemas veiculares;
1.2. Evolução da eletrônica embarcada;
1.3. Tendências tecnológicas e o futuro;
2. Componentes automotivos básicos
2.1. Introdução;
2.2. Componentes fundamentais: sensores, atuadores, unidade eletrônica de controle (ECU);
2.3. Componentes auxiliares: fusíveis, relés, chicote, interruptores; 
2.4. Diagramas elétricos;
3. Baterias automotivas
3.1. Requisitos funcionais;
3.2. Escolha da bateria;
3.3. Parâmetros de uma bateria; 
3.4. Aspectos construtivos;
4. Sistemas de carregamento
4.1. Principio de funcionamento;
4.2. Componentes: alternador, retificador, regulador de tensão
4.3. Circuito de alerta e indicação de mau funcionamento;
5.	Sistema de partida do motor
5.1. Princípio de funcionamento;
5.2. Principais componentes;
5.3. Circuito equivalente do sistema;
5.4. Motor de partida;
6. Sistema de ignição
6.1. Princípio de funcionamento;
6.2. Principais componentes;
6.3. Ângulo de ignição e de posicionamento;
6.4. Circuito equivalente;
6.5. Tipos de ignição: convencional, eletrônica, programada e sem distribuidor;
7. Controle da injeção e gerenciamento do motor
7.1. Evolução dos sistemas de injeção de combustível
7.2. Injeção eletrônica de combustível;
7.3. Componentes do sistema de injeção eletrônica;
7.4. Sistema de gerenciamento do motor;
7.5. Controles de emissão;
7.6. Componentes para controle de emissão;
7.7. Injeção eletrônica de combustível - sistema Jetronic (L-Jetronic, K-Jetronic e Mono-Jetronic);
7.8. Gerenciamento do motor - sistema Motronic (M-Motronic, ME-Motronic e MED-Motronic);
8. Sistema de iluminação automotiva
8.1. Conceitos básicos de iluminação;
8.2. Lâmpadas automotivas;
8.3. Refletores automotivos;
8.4. Outros sistemas de iluminação;
9.	Sistemas elétricos do chassi
9.1. Sistema ABS; 
9.2. Suspensão ativa ;
9.3. Sistema de controle de tração;
9.4. Transmissão automática;
9.5. Airbag
9.6. Sistemas avançados de assitência ao condutor (ADAS);
10.	Diagnóstico de bordo
10.1. História e evolução dos sitemas de diagnósticos automotivos;
10.2. Instrumentos e equipamentos para diagnóstico veicular;
10.3. Diagnóstico de bordo: OBDII;
10.4. Nomenclatura para formação de códigos de falhas (DTC);
10.5. Leitura e interpretação de DTCs;
11.	Introdução a redes veiculares
11.1. Conceitos básicos;
11.2. Arquiteturas elétricas;
11.3. Classe de redes automotivas;
11.4. Introdução a rede CAN;
11.5. Introdução ao padrão SAE J1939
[Bibliografia Básica]
Tom Denton, Automobile electrical and electronic system, 4ª. Ed., Routledge, 2012.  
Dominique Paret (2007). Multiplexed Networks for Embedded Systems – CAN, LIN, FlexRay, Safe-by-Wire. Editora Wiley. 
Robert Bosch GmbH, Bosch Automotive Handbook, 7º Edição. 
[Bibliografia Complementar]
Tom Denton, Advanced Automotive Fault Diagnosis, 1ª. Ed., Rio de Janeiro, Butterworth-Heineman, 2011.  
Max M. D. Santos (2010). Redes de Comunicação Automotiva - Características, Tecnologias e Aplicações. 1ª Edição. Editora Érica. 
Allan W. M. Bonnick, Automotive Computer Controlled Systems, Editora Butterworth-Heinemann, 2001 
Alexandre A. Guimarães, Eletrônica Embarcada Automotiva, Editora Érica, 2007
[Pré-requisito]
Circuitos Eletrônicos 1 ou Eletricidade Aplicada

