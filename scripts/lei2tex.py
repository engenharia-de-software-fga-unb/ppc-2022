# !/usr/bin/python
# -*- coding: utf-8 -*-

import sys


states = ('NONE', 'TEXTO', 'UNICO', 'ARTIGO', 'PARAGRAFO', 'INCISO', 'ALINEA'
    'SECAO', 'CAPITULO')


enter_state = {
    'NONE' : '',
    'ARTIGO' : '\\artigo{',
    'PARAGRAFO' : '\\leipar{',
    'INCISO' : '\\inciso{',
    'ALINEA' : '\\alinea{',
    'UNICO': 'Parágrafo Único.',
    'SECAO': '\\secao{',
    'CAPITULO': '\\capitulo{',
    'TEXTO' : '',
}


exit_state = {
    'NONE' : '',
    'ARTIGO' : '}',
    'PARAGRAFO' : '}',
    'INCISO' : '}',
    'ALINEA' : '}',
    'UNICO' : '',
    'SECAO' : '}',
    'CAPITULO': '}',
    'TEXTO' : '',
}


roman = ('I', 'II', 'III', 'IV', 'V', 'VI', 'VII', 'VIII', 'IX', 'X', 'XI', 'XII', 'XIII', 'XIV', 'XV', 'XVI', 'XVII', 'XVIII', 'XIX', 'XX')

def get_state(header, text):

#    print 'header = [{}]'.format(header)

    text = text.replace('%', '\\%')
    text = text.replace('$', '\\$')

    if header.strip()[0] == '"':
        text = header + ' ' + text
        return 'TEXTO', text
    if 'art' in header.lower():
        if ' ' not in text:
            pos = text.find('.')
            text = text[pos + 1:]
        else:
            _, text = text.split(' ', 1)

        return 'ARTIGO', text
    elif '§' in header:
        _, text = text.split(' ', 1)
        return 'PARAGRAFO', text
    elif header in roman:
        _, text = text.split(' ', 1)
        return 'INCISO', text
    elif ')' in header:
        if 'NR' in header:
            text = header + ' ' + text
            return 'TEXTO', text
        else:
            _, text = text.split(' ', 1)
            return 'ALINEA', text
    elif 'Parágrafo' in header:
        _, text = text.split(' ', 1)
        return 'UNICO', text
    elif 'seção' in header.strip().lower():
        return 'SECAO', ''
    elif 'CAPÍTULO' in header.strip().upper():
        return 'CAPITULO', ''
    else:
        text = header + ' ' + text
        return 'TEXTO', text


if __name__ == '__main__':

    if len(sys.argv) < 2:
        print 'Usage: {} lei.txt'
        sys.exit(-1)

    with open(sys.argv[1]) as f:
        lines = [x.strip() for x in f.readlines()]

    open_capitulo = False
    open_secao = False

    for line in lines:

        if len(line) == 0:
            continue;

        # print 'lower line = [{}]'.format(line)
        if ' ' in line:
            header, text = line.split(' ', 1)
        else:
            header = line
            text = ''

        #print 'header = [{}]'.format(header)
        #print 'text = [{}]'.format(text)
        state, text = get_state(header, text)

        if state != 'TEXTO' and open_secao:
            open_secao = False
            print exit_state['SECAO']

        if state != 'TEXTO' and open_capitulo:
            open_capitulo = False
            print exit_state['CAPITULO']

        print enter_state[state],

        if state == 'SECAO':
            open_secao = True
            continue

        if state == 'CAPITULO':
            open_capitulo = True
            continue

        print text,

        print exit_state[state]
