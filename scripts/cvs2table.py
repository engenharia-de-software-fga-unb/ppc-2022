# !/usr/bin/python
# -*- coding: utf-8 -*-

import glob


def read(datafile):

    with open(datafile) as f:
        data = f.readlines()

    return data


def createTable(data, label, caption):

    tex = []

    tex.append('\\begin{table}[h!tp]')
    tex.append('\\centering')

    if len(caption) > 0:
        tex.append('\\caption{%s}' % caption)

    tex.append('\\label{%s}' % label)
    
    specs = data[0].strip().split(',')
    width = specs[0]
    specs.pop(0)
 
    data.pop(0)
    tex.append('\\begin{tabularx}{{%s}\\textwidth}{%s}' % (width, ''.join(specs)))

    rule = '\\toprule'
    first = True

    for info in data:
        tex.append(rule)
        blocks = info.replace('%','\\%').strip().split('"')
        fields = []

        for i in range(len(blocks)):
            if i % 2 == 0:
                fields.extend(blocks[i].split(','))
            else:
                fields.extend([blocks[i]])

        if first:
            fields = ['\\textbf{%s}' % field for field in fields if len(field) > 0]
        else:
            fields = [field for field in fields if len(field) > 0]

        tex.append('{}\\\\'.format(' & '.join(fields)))
        rule = '\\midrule'
        first = False

    tex.append('\\bottomrule')
    tex.append('\\end{tabularx}')
    tex.append('\\end{table}')

    return tex


def createBox(data, label, caption):

    tex = []

    tex.append('\\begin{quadro}[h!tp]')
    tex.append('\\centering')
    tex.append('\\caption{%s}' % caption)
    tex.append('\\label{%s}' % label)
    
    specs = data[0].strip().split(',')
    data.pop(0)
    tex.append('\\begin{tabularx}{0.95\\textwidth}{|%s|}' % '|'.join(specs))
    tex.append('\\hline')

    for info in data:
        blocks = info.replace('%','\\%').strip().split('"')
        fields = []

        for i in range(len(blocks)):
            if i % 2 == 0:
                fields.extend(blocks[i].split(','))
            else:
                fields.extend([blocks[i]])

        fields = [field for field in fields if len(field) > 0]
        tex.append('{}\\\\'.format(' & '.join(fields)))
        tex.append('\\hline')

    tex.append('\\end{tabularx}')
    tex.append('\\end{quadro}')

    return tex


def cvs2latex(data):

    label, caption = data[0].strip().split(',')
    data.pop(0)

    out, _ = label.split(':')

    if out == 'box':
        return createBox(data, label, caption)
    else:    
        return createTable(data, label, caption)


def output_filename(datafile):

    _, filename = datafile.split('/')
    name, _ = filename.split('.')

    return '{}/{}.tex'.format('tabelas', name)


def save(tex, texfile):

    with open(texfile, 'w') as f:
        f.write('\n'.join(tex))


if __name__ == '__main__':

    for datafile in glob.glob('dados/*.csv'):
        data = read(datafile)
        tex = cvs2latex(data)
        texfile = output_filename(datafile)
        save(tex, texfile)

