import os
import argparse

p = argparse.ArgumentParser()
secs = ['[Ementa]', '[Programa]', '[Bibliografia Básica]', 
        '[Bibliografia Complementar]', '[Pre-requisito]']

def data(path):
    with open(path) as F:
        return F.read()

def missing_secs(data):
    for sec in secs:
        _, sec, data = data.partition(sec)
        if not sec:
            return sec
            
def sec(data, sec):
    pre, sep, data = data.partition(sec)
    if not sep:
        print(data)
        print(sec)
        raise ValueError('could not find section: %s' % sec)
    L = []
    for line in data.splitlines():
        if line.startswith('[') and line.endswith(']'):
            break
        L.append(line)
    return ('\n'.join(L)).strip()
            
def validate(path, data):
    print(path)
    
    # Contem todas secoes?
    miss = missing_secs(data)
    if miss:
        raise SystemExit('%s: missing %s' % (path, data))
        
    # Ementa ou programa vazios?
    if sec(data, '[Ementa]') == '':
        raise SystemExit('%s: empty [Ementa]' % path)
    if sec(data, '[Programa]') == '':
        print('    empty [Programa]')
    
    # Bibliografia basica < 3 ou complementar < 5?
    basica = map(lambda x: x.strip(), sec(data, '[Bibliografia Básica]').splitlines())
    N = sum(1 for x in basica if x)
    if N < 3:
        print('    Bibliografia Básica: only %s entries' % N)
    comp = map(lambda x: x.strip(), sec(data, '[Bibliografia Complementar]').splitlines())
    N = sum(1 for x in comp if x)
    if N < 5:
        print('    Bibliografia Complementar: only %s entries' % N)

    # CAPS?    
    for sec_ in secs[:-1]:
        words = sec(data, sec_).split()
        n_all = len(words)
        n_upper = sum(1 for w in words if w.isupper())
        if n_upper > n_all * 0.80:
            print('    %s: ALL CAPS?' % sec_)
        
    # TODOs?
    if 'to-do' in data or 'TO-DO' in data or 'TODO' in data:
        print('    TO-DO')
    
if __name__ == '__main__':
    # args = p.parse_args()
    files = os.listdir(os.getcwd())
    for path in sorted(files):
        validate(path, data(path))
