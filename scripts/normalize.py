import os
import argparse

p = argparse.ArgumentParser()
p.add_argument('file')
p.add_argument('--save', '-s', action='store_true')

def data(path):
    with open(path) as F:
        return F.read()

def split_ementa(data):
    pre, sep, post = data.partition('[Ementa]\n')
    post = sep + post
    ementa, sep, post = post.partition('[Programa]\n')
    post = sep + post
    return pre, ementa, post

def remove_bullets(data):
    def fix(x):
        if x.endswith(';'):
            x = x.rstrip(' ;') + '.' 
        n, sep, tail = x.partition('. ')
        if sep and (n.isdigit() or set(n).issubset(set('IVX'))):
            return tail.strip()
        else:
            return x
        
    L = []
    for line in data.splitlines():
        L.append(fix(line))
    return '\n'.join(L)

def fix_ementa(data):
    title, ementa, tail = split_ementa(data)
    ementa = remove_bullets(ementa)
    return title + ementa + '\n' + tail

def space_before_sections(data):
    return data.replace('\n[', '\n\n[').replace('\n\n\n[', '\n\n[')


def subs(data, D):
    for k, v in D.items():
        data = data.replace(k, v)
    return data


def transform_file(path):
    d = data(path)
    lines = map(lambda x: x.rstrip(), d.splitlines())
    lines = [l for l in lines if l]
    d = '\n'.join(lines)
    
    d = fix_ementa(d)
    d = space_before_sections(d)
    d = d.replace('  ', ' ').replace('\t', ' ')
    d = subs(d, {
        'EBRARY': 'eBrary',
        'eBrasy': 'eBrary',
        'Ebrary': 'eBrary',
        'OPEN ACCESS': 'open access',
        'Open Access': 'open access',
        'Open Acess': 'open access',
        'openaccess': 'open access',
        '[open access]': '(open access)',
    })
    return d

if __name__ == '__main__':
    args = p.parse_args()
    f = args.file 
    if f == 'all':
        files = os.listdir(os.getcwd())
    else:
        files = [f]
        
    for path in files:
        d = transform_file(path)
        
        if args.save:    
            with open(path, 'w') as F:
                F.write(d)
        else:
            print(d)
            print('-' * 80)
