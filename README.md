# Template PPC's da FGA

Template para os novo Projeto Pedagógicos do Cursos (PPC) Faculdade do
Gama (FGA) em Latex.

Licenciado em Creative Commons Atribuição 3.0:
http://creativecommons.org/licenses/by/3.0/

Desenvolvido e adaptado pelo professor Edson Alves <edsomjr@gmail.com>. 

# Estrutura do documento

## Ementa de disciplinas

A edição da ementa das disciplinas foi simplificada e deve ser atualizadas diretamente nos arquivos '.txt' nas pastas `_ini`.

Neste caso, os arquivos se encontram em:

Disciplinas compartilhadas e do tronco comum: `disciplinas/_ini`
Disciplinas específicas do curso de software: `disciplinas/software/_ini`

As tabelas em `.tex` nas respectivas pastas `tabelas` são geradas automaticamente pelo script no Makefile e não devem ser editadas manualmente.

# Instalando as dependências manualmente para a geração do documento

Os pré-requisistos para gerar o documento são o Latex, mais especificamente o `abnTeX2` e o uso do `make`e o `pyhton`

### abnTeX2

O pacote abnTeX2 está disponível no Github https://github.com/abntex/abntex2 onde estão disponíveis as instruções de instalação nativas para cada plataforma (Linux/ macOS, Windows).

### python

A geração do PDF foi testada tanto com as versões de `python2` e `python3`.
O Makefile está configurado para usar o `python3` como padrão. Para executar com o python2, basta usar o comando:

```
make python2
```

### Compilando

Para compilar o texto através do Makefile digite:

```
$ make clean
$ make
```

# Solução Dockerizada

Foi criada uma alternativa à instalar e configurar o ambiente do Latex, make e python utilizando o docker. Neste sentido, é necessário instalar a ferramenta 'docker' (https://www.docker.com/get-started/) e em seguida preparar a imagem do container através do seguinte comando:

```
docker build -t pdflatex .
```

ou rodando o script `gera_imagem_docker.sh`

```
bash gera_imagem_docker
```

Obs: a imagem do container terá aprox 6 GB pois contém todo o ambiente do abnTeX2.

Após ter o container gerado, basta rodar o scipt `gerar_pdf.sh`
```
bash gerar_pdf
```