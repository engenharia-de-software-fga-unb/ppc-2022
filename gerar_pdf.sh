#!/usr/bin/env bash

docker run --rm -v "$PWD":/usr/src/project -w /usr/src/project pdflatex make clean
docker run --rm -v "$PWD":/usr/src/project -w /usr/src/project pdflatex make python2